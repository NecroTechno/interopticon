import express from "express";
import bodyParser from "body-parser";
import "regenerator-runtime/runtime.js";
import * as db from "./queries";
import * as utils from "./utils";
import * as auth from "./auth";

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

utils.initialUserSetup();

app.get("/", (request, response) => {
  response.json({ info: "OK" });
});

app.post("/login", auth.login);

app.post("/hosts", auth.tokenValidatior, (request, response) => {
  try {
    const { count } = request.body;
    const { offset } = request.body;
    db.getHosts(count, offset, (success, err, hosts) => {
      if (!success) {
        response.status(500).send();
      } else {
        response.status(200).json({ hosts: hosts });
      }
    });
  } catch {
    response.status(500).send();
  }
});

app.post("/ports", auth.tokenValidatior, (request, response) => {
  try {
    const { ip } = request.body;
    db.getPorts(ip, (success, _err, ports) => {
      if (!success) {
        response.status(500).send();
      } else {
        response.status(200).json({ ports: ports });
      }
    });
  } catch {
    response.status(500).send();
  }
});

app.post("/search", auth.tokenValidatior, (request, response) => {
  try {
    const { searchTerm } = request.body;
    const { count } = request.body;
    const { offset } = request.body;

    if (searchTerm.includes("$")) {
      let qb = new db.QueryBuilder(searchTerm);
      qb.parseQuery((success, query) => {
        if (!success) {
          response.status(500).send('Unable to parse query.');
        } else {
          db.searchAdvanced(query, count, offset, (success, _err, hosts) => {
            if (!success) {
              response.status(500).send();
            } else {
              response.status(200).json({ hosts: hosts });
            }
          });
        }
      });
    } else {
      db.searchHosts(searchTerm, count, offset, (success, _err, hosts) => {
        if (!success) {
          response.status(500).send();
        } else {
          response.status(200).json({ hosts: hosts });
        }
      });
    }
  } catch {
    response.status(500).send();
  }
});

app.get("/details", auth.tokenValidatior, (_request, response) => {
  db.hostsCount((success, _err, count) => {
    if (!success) {
      response.status(500).send();
    } else {
      response.status(200).json({ hostCount: parseInt(count[0].count) });
    }
  })
})

app.use(function (req, res, next) {
  res.status(404).send();
});

app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
