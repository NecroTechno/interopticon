import * as db from "./queries";
import bcrypt from "bcrypt";
import jsonwebtoken from "jsonwebtoken";

// probably should have this expire....
const signJwtToken = (username) => {
  let jwtSecretKey = process.env.JWT_SECRET_KEY;
  let data = {
    time: Date(),
    userId: username,
  };
  return jsonwebtoken.sign(data, jwtSecretKey);
};

const verifyJwtToken = (token) => {
  try {
    let jwtSecretKey = process.env.JWT_SECRET_KEY;
    return jsonwebtoken.verify(token, jwtSecretKey);
  } catch {
    return false;
  }
};

const login = (request, response) => {
  try {
    const { username, password } = request.body;
    db.getUserById(username, (success, _err, userData) => {
      if (!success) {
        response.status(500);
      } else if (userData.length == 0) {
        response.status(400).json({ info: "User does not exist!" });
      } else {
        bcrypt.compare(password, userData[0].password, (err, result) => {
          if (err) {
            response.status(500);
          } else if (!result) {
            response.status(401).json({ info: "Incorrect password" });
          } else {
            response.status(200).json({ token: signJwtToken(username) });
          }
        });
      }
    });
  } catch {
    response.status(500);
  }
};

const tokenValidatior = (request, response, next) => {
  let token = request.header("Authorization");
  if (!token) {
    response.status(403).json({ error: "Authorisation token not sent!" });
  } else {
    if (verifyJwtToken(token)) {
      next();
    } else {
      response.status(403).json({ error: "Authorisation token invalid!" });
    }
  }
};

export { login, tokenValidatior };
