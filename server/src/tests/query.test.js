import { QueryBuilder } from "../queries";

test('tests advanced query', () => {
    const searchTerm = "$PORT 80 and tcp"
    let qb = new QueryBuilder(searchTerm);

    qb.parseQuery((_success, query) => {
        expect(query).toBe("port = 80 AND name LIKE '%tcp%' OR state LIKE '%tcp%' OR service LIKE '%tcp%' OR info LIKE '%tcp%'");
    });

    
});

test('tests advanced query fail (and first)', () => {
    const searchTerm = "and tcp long query"
    let qb = new QueryBuilder(searchTerm);

    qb.parseQuery((success, _query) => {
        expect(success).toBe(false);
    });
});

test('tests advanced query (port must be int)', () => {
    const searchTerm = "$PORT fail"
    let qb = new QueryBuilder(searchTerm);

    qb.parseQuery((success, _query) => {
        expect(success).toBe(false);
    });
});