import * as db from "./queries";
import bcrypt from "bcrypt";

// handle initial server user auth
function initialUserSetup() {
  if (!!process.env.SERVER_USER && !!process.env.SERVER_PASSWORD) {
    console.log("Default server user env variables found, creating user now.");
    try {
      db.getUserById(process.env.SERVER_USER, (success, err, userRows = []) => {
        if (!success) {
          throw(err)
        }
        if (userRows.length > 0) {
          console.log("Default server user already exists.");
        } else {
          bcrypt.hash(process.env.SERVER_PASSWORD, 10, (err, hash) => {
            if (err) {
              throw(err)
            }
            db.newUser(process.env.SERVER_USER, hash, (success, err) => {
              if (!success) {
                throw(err)
              }
              console.log("New server user created");
            });
          });
        }
      });
    } catch (e) {
      console.warn("Unable to create new user: ", e);
    }
  }
}

export { initialUserSetup };
