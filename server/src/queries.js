const Pool = require("pg").Pool;
const pool = new Pool({
  user: process.env.POSTGRES_USER,
  host: process.env.DATABASE_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  port: 5432,
});

class QueryBuilder {
  constructor(searchTerm) {
    this.queryTerms = searchTerm.split(" ").filter(e => e);
    this.skipNextTerm = false;
    this.greedySearch = false;
  }

  toggleSkip = () => {
    this.skipNextTerm = !this.skipNextTerm;
  }

  parseQuery = (cb) => {
    let queries = [];

    try {
      this.queryTerms.forEach((element, index) => {
        if (this.greedySearch) {
          return
        }
  
        if (this.skipNextTerm) { // the term as already been parsed
          this.toggleSkip();
          return
        }
  
        const fail = (_message = null) => {
          throw ("Unable to parse: " + (!!message ? message : "generic"));
        }
  
        if (element.includes("$")) {
          let nextTerm = this.queryTerms[index + 1];
          if (!nextTerm && nextTerm !== "and") {
            fail()
          }
          switch (element) {
            case "$PORT":
              if (!parseInt(nextTerm)) {
                fail("port must be int")
              }
              queries.push(`port = ${nextTerm}`);
              this.toggleSkip();
              break;
            default:
              fail()
          }
        } else if (element.toLowerCase() == "and" || element.toLowerCase() == "or") {
          let nextTerm = this.queryTerms[index + 1];
          if (queries.length == 0 || !nextTerm) {
            fail()
          }
          queries.push(element.toUpperCase())
        } else {
          let fullText = "%" + this.queryTerms.slice(index).join(" ") + "%";
          queries.push(`(name LIKE '${fullText}' OR state LIKE '${fullText}' OR service LIKE '${fullText}' OR info LIKE '${fullText}')`);
          this.greedySearch = true;
        }
      });
      
      cb(true, queries.join(" "));
    } catch(_e) {
      cb(false)
    }
  }
}

const getUserById = (username, cb) => {
  return pool.query(
    "SELECT * FROM users WHERE username = $1",
    [username],
    (error, results) => {
      if (error || !results) {
        cb(false, error);
      }
      cb(true, null, results.rows);
    }
  );
};

const newUser = (username, hashed_pw, cb) => {
  return pool.query(
    "INSERT INTO users(username, password) VALUES($1, $2) RETURNING username",
    [username, hashed_pw],
    (error, results) => {
      if (error || !results) {
        cb(false, error);
      }
      cb(true, null, results.rows);
    }
  );
};

const getHosts = (count, offset, cb) => {
  return pool.query(
    "SELECT * FROM hosts ORDER BY last_updated DESC LIMIT $1 OFFSET $2",
    [count, offset],
    (error, results) => {
      if (error || !results) {
        cb(false, error);
      }
      cb(true, null, results.rows);
    }
  );
};

const getPorts = (ip, cb) => {
  return pool.query(
    "SELECT * FROM ports WHERE ip = $1",
    [ip],
    (error, results) => {
      if (error || !results) {
        cb(false, error);
      }
      cb(true, null, results.rows);
    }
  );
};

const searchHosts = (searchTerm, count, offset, cb) => {
  return pool.query(
    "SELECT * FROM hosts WHERE ip IN (SELECT DISTINCT ip FROM ports WHERE name LIKE $1 OR state LIKE $1 OR service LIKE $1 OR info LIKE $1) OR os LIKE $1 ORDER BY last_updated DESC LIMIT $2 OFFSET $3",
    ["%" + searchTerm + "%", count, offset],
    (error, results) => {
      if (error || !results) {
        cb(false, error);
      }
      cb(true, null, results.rows);
    }
  );
};

const searchAdvanced = (query, count, offset, cb) => {
  let params = [query, count, offset];

  let preparedQuery = `SELECT * FROM hosts WHERE ip IN (SELECT DISTINCT ip FROM ports WHERE ${params[0]}) ORDER BY last_updated DESC LIMIT ${params[1]} OFFSET ${params[2]}`

  return pool.query(preparedQuery,
    (error, results) => {
      if (error || !results) {
        cb(false, error);
      }
      cb(true, null, results.rows);
    }
  );
};

const hostsCount = (cb) => {
  return pool.query("SELECT COUNT(*) FROM hosts", (error, results) => {
    if (error || !results) {
      cb(false, error);
    }
    cb(true, null, results.rows);
  });
};

export { getUserById, newUser, getHosts, getPorts, searchHosts, searchAdvanced, hostsCount, QueryBuilder };
