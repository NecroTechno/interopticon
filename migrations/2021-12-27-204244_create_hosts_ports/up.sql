CREATE TABLE IF NOT EXISTS hosts(
  ip VARCHAR PRIMARY KEY NOT NULL,
  last_updated TIMESTAMP NOT NULL,
  hostname VARCHAR NOT NULL,
  protocol VARCHAR NOT NULL,
  os VARCHAR NOT NULL,
  os_accuracy INT NOT NULL,
  state VARCHAR NOT NULL,
  whois TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS ports(
  ip VARCHAR NOT NULL,
  port INT NOT NULL,
  protocol VARCHAR NOT NULL,
  name VARCHAR,
  state VARCHAR,
  service TEXT,
  info TEXT,
  PRIMARY KEY (ip, port, protocol)
);