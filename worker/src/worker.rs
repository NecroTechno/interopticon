use std::process::Command;
use std::time::SystemTime;

use crate::types::{Host, Port, ScanResults};

fn service_details(node: roxmltree::Node) -> Option<String> {
    let mut service_vec: Vec<String> = Vec::new();

    let product = node.attribute("product").map(|descr| descr.to_string());
    let version = node.attribute("version").map(|ver| ver.to_string());
    let extrainfo = node.attribute("extrainfo").map(|extra| extra.to_string());

    [product, version, extrainfo].into_iter().for_each(|attr| {
        if let Some(attr_string) = attr {
            service_vec.push(attr_string)
        }
    });

    if !service_vec.is_empty() {
        let service_string = service_vec.join(" ");
        Some(service_string)
    } else {
        None
    }
}

fn info_details(nodes: Vec<roxmltree::Node>) -> Option<String> {
    let mut info_vec: Vec<String> = Vec::new();

    nodes.into_iter().for_each(|node| {
        let id = node.attribute("id").map(|id| id.to_string());
        let output = node.attribute("output").map(|output| output.to_string());
        if id.is_some() && output.is_some() {
            info_vec.push(format!("{}: {}\n", id.unwrap(), output.unwrap()));
        }
    });

    if !info_vec.is_empty() {
        let info_string = info_vec.join(" ");
        Some(info_string)
    } else {
        None
    }
}

// improve error handling
pub fn worker(ipaddr: String, scanspeed: &str) -> std::option::Option<ScanResults> {
    let output = match Command::new("nmap")
        .args(["-sV", "-Pn", "-sC", "-A", "-O", scanspeed, "-oX", "-", &ipaddr])
        .output()
    {
        Ok(res) => res,
        Err(_) => return None,
    };

    let doc = match roxmltree::Document::parse(match std::str::from_utf8(&output.stdout) {
        Ok(res) => res,
        Err(_) => return None,
    }) {
        Ok(res) => res,
        Err(_) => return None,
    };
    let whois_res = match Command::new("whois").arg(&ipaddr).output() {
        Ok(res) => res.stdout,
        Err(_) => return None,
    };

    let ip = match doc.descendants().find(|n| n.tag_name().name() == "address") {
        Some(res) => match res.attribute("addr") {
            Some(res) => res,
            None => return None,
        },
        None => return None,
    };
    let hostname = match doc
        .descendants()
        .find(|n| n.tag_name().name() == "hostname")
    {
        Some(res) => match res.attribute("name") {
            Some(res) => Some(res.to_owned()),
            None => None,
        },
        None => None,
    };
    let protocol = match doc
        .descendants()
        .find(|n| n.tag_name().name() == "scaninfo")
    {
        Some(res) => match res.attribute("protocol") {
            Some(res) => res,
            None => return None,
        },
        None => return None,
    };
    let os = match doc.descendants().find(|n| n.tag_name().name() == "osmatch") {
        Some(res) => match res.attribute("name") {
            Some(res) => res,
            None => return None,
        },
        None => return None,
    };
    let os_accuracy = match doc.descendants().find(|n| n.tag_name().name() == "osmatch") {
        Some(res) => match res.attribute("accuracy") {
            Some(res) => res,
            None => return None,
        },
        None => return None,
    };
    let state = match doc.descendants().find(|n| n.tag_name().name() == "status") {
        Some(res) => match res.attribute("state") {
            Some(res) => res,
            None => return None,
        },
        None => return None,
    };
    let whois = match std::str::from_utf8(&whois_res) {
        Ok(res) => res,
        Err(_) => return None,
    };

    let host = Host {
        ip: ip.to_string(),
        last_updated: SystemTime::now(),
        hostname: hostname,
        protocol: protocol.to_string(),
        os: os.to_string(),
        os_accuracy: os_accuracy.parse::<i32>().unwrap(),
        state: state.to_string(),
        whois: whois.to_string(),
    };

    let mut ports: Vec<Port> = Vec::new();

    doc.descendants().for_each(|n| {
        if n.tag_name().name() == "port" {
            let port = match n.attribute("portid") {
                Some(res) => res,
                None => return,
            };
            let protocol = match n.attribute("protocol") {
                Some(res) => res,
                None => return,
            };
            let name = match n.descendants().find(|n| n.tag_name().name() == "service") {
                Some(node) => node.attribute("name").map(|name| name.to_string()),
                None => None,
            };
            let state = match n.descendants().find(|n| n.tag_name().name() == "state") {
                Some(node) => node.attribute("state").map(|state| state.to_string()),
                None => None,
            };
            let service = n
                .descendants()
                .find(|n| n.tag_name().name() == "service")
                .map(service_details)
                .flatten();
            let scripts: Vec<roxmltree::Node> = n
                .descendants()
                .filter(|n| n.tag_name().name() == "script")
                .collect();
            let info = info_details(scripts);

            ports.push(Port {
                ip: ip.to_string(),
                port: port.parse::<i32>().unwrap(),
                protocol: protocol.to_string(),
                name,
                state,
                service,
                info,
            });
        };
    });

    Some(ScanResults { host, ports })
}
