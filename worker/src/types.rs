use std::time::SystemTime;

use crate::schema::hosts;
use crate::schema::ports;

#[derive(Debug)]
pub struct Host {
    pub ip: String,
    pub last_updated: SystemTime,
    pub hostname: Option<String>,
    pub protocol: String,
    pub os: String,
    pub os_accuracy: i32,
    pub state: String,
    pub whois: String,
}

#[derive(Debug)]
pub struct Port {
    pub ip: String,
    pub port: i32,
    pub protocol: String,
    pub name: Option<String>,
    pub state: Option<String>,
    pub service: Option<String>,
    pub info: Option<String>,
}

#[derive(Debug)]
pub struct ScanResults {
    pub host: Host,
    pub ports: Vec<Port>,
}

#[derive(Debug, Insertable)]
#[table_name = "hosts"]
pub struct NewHost<'a> {
    pub ip: &'a str,
    pub last_updated: &'a SystemTime,
    pub hostname: Option<String>,
    pub protocol: &'a str,
    pub os: &'a str,
    pub os_accuracy: &'a i32,
    pub state: &'a str,
    pub whois: &'a str,
}

#[derive(Debug, Insertable)]
#[table_name = "ports"]
pub struct NewPort<'a> {
    pub ip: &'a str,
    pub port: &'a i32,
    pub protocol: &'a str,
    pub name: Option<String>,
    pub state: Option<String>,
    pub service: Option<String>,
    pub info: Option<String>,
}
