use diesel::pg::PgConnection;
use diesel::prelude::*;
use std::env;

use crate::types::{Host, NewHost, NewPort, Port};
// use crate::schema::hosts::{ip, last_updated, hostname, protocol, os, os_accuracy, state, whois};
// use crate::schema::ports::{ip, port, protocol, name, state, service, info};

pub fn establish_connection() -> PgConnection {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub fn create_host(conn: &PgConnection, host: Host) {
    use crate::schema::hosts;

    let new_host = NewHost {
        ip: &host.ip,
        last_updated: &host.last_updated,
        hostname: host.hostname,
        protocol: &host.protocol,
        os: &host.os,
        os_accuracy: &host.os_accuracy,
        state: &host.state,
        whois: &host.whois,
    };

    diesel::insert_into(hosts::table)
        .values(&new_host)
        .on_conflict(hosts::ip)
        .do_update()
        .set((
            hosts::last_updated.eq(&new_host.last_updated),
            hosts::hostname.eq(&new_host.hostname),
            hosts::protocol.eq(&new_host.protocol),
            hosts::os.eq(&new_host.os),
            hosts::os_accuracy.eq(&new_host.os_accuracy),
            hosts::state.eq(&new_host.state),
            hosts::whois.eq(&new_host.whois)
        ))
        .execute(conn)
        .expect("Error saving new host");
}

pub fn create_port(conn: &PgConnection, port: Port) {
    use crate::schema::ports;

    let new_port = NewPort {
        ip: &port.ip,
        port: &port.port,
        protocol: &port.protocol,
        name: port.name,
        state: port.state,
        service: port.service,
        info: port.info,
    };

    diesel::insert_into(ports::table)
        .values(&new_port)
        .on_conflict((ports::ip, ports::port, ports::protocol))
        .do_update()
        .set((
            ports::port.eq(&new_port.port),
            ports::protocol.eq(&new_port.protocol),
            ports::name.eq(&new_port.name),
            ports::state.eq(&new_port.state),
            ports::service.eq(&new_port.service),
            ports::info.eq(&new_port.info),
        ))
        .execute(conn)
        .expect("Error saving new host");
}
