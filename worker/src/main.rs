mod db;
mod schema;
mod types;
mod worker;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

use rand::Rng;

use std::env;
use std::sync::mpsc::channel;
use std::thread;

// should be more discerning
fn generate_ip() -> String {
    let mut octets: Vec<u32> = Vec::new();

    for _i in 0..4 {
        octets.push(rand::thread_rng().gen_range(0..255))
    }

    let mut ipaddr = octets
        .iter()
        .map(|x| x.to_string() + ".")
        .collect::<String>();
    ipaddr = ipaddr.trim_end_matches('.').to_string();

    ipaddr
}

embed_migrations!();

fn main() {
    let connection = db::establish_connection();
    embedded_migrations::run(&connection).expect("failed to run migrations");

    println!("Worker running!");

    let (tx, rx) = channel();

    //namp timing template
    let scanspeed = match env::var("SCANSPEED") {
        Ok(res) => match res.as_str() {
            "0" => "-T0".to_string(),
            "1" => "-T1".to_string(),
            "2" => "-T2".to_string(),
            "3" => "-T3".to_string(),
            "4" => "-T4".to_string(),
            "5" => "-T5".to_string(),
            _ => "-T3".to_string(),
        },
        Err(_) => "-T3".to_string(),
    };

    let worker_count: u32 = match env::var("WORKER_COUNT") {
        Ok(res) => match res.parse::<u32>() {
            Ok(wc) => wc,
            Err(_) => 10,
        },
        Err(_) => {
            println!("Worker count unset!");
            10
        }
    };

    for _i in 0..worker_count {
        let tx = tx.clone();
        let sp_clone = scanspeed.clone();
        thread::spawn(move || loop {
            tx.send(worker::worker(generate_ip(), &sp_clone)).unwrap();
        });
    }

    loop {
        match rx.recv() {
            Ok(scan_results) => {
                if let Some(sr) = scan_results {
                    println!("Host found: {:?}", sr.host.ip);
                    db::create_host(&connection, sr.host);
                    sr.ports.into_iter().for_each(|p| {
                        db::create_port(&connection, p);
                    })
                }
            }
            Err(_) => {
                panic!("uh oh")
            }
        }
    }
}
