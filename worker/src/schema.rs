table! {
    hosts (ip) {
        ip -> Varchar,
        last_updated -> Timestamp,
        hostname -> Nullable<Varchar>,
        protocol -> Varchar,
        os -> Varchar,
        os_accuracy -> Int4,
        state -> Varchar,
        whois -> Text,
    }
}

table! {
    ports (ip, port, protocol) {
        ip -> Varchar,
        port -> Int4,
        protocol -> Varchar,
        name -> Nullable<Varchar>,
        state -> Nullable<Varchar>,
        service -> Nullable<Text>,
        info -> Nullable<Text>,
    }
}

table! {
    users (username) {
        username -> Varchar,
        password -> Varchar,
    }
}

allow_tables_to_appear_in_same_query!(
    hosts,
    ports,
    users,
);
