# Interopticon

Port scanner, database, and API.

## Legal

It's VERY LIKELY that mass port scanning is illegal in your jurisdiction. This is a proof of concept. Do not go around mass port scanning unless you want trouble.

## Environment variables

- `POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_DB`: all used to setup the postgres db
- `DATABASE_URL`: for diesel to run migrations
- `SCANSPEED`: for nmap timing argument. defaults to 3
- `WORKER_COUNT`: for determining how many threads should crawl. defaults to 10
- `JWT_SECRET_KEY`: for signing JWT in the server

## Todo

### Worker
- improve host crawling
- refactor xml parsing

### Server
- Convert queries from callback to promise